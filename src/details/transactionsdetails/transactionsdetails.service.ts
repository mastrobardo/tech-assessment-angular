import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../../enums/enums';
import { Transaction } from '../shared/interfaces/transactions.interface';

@Injectable({
  providedIn: 'root'
})
export class TransactionsDetailsService {

  constructor( private http: HttpClient ) { }
 
  fetchTransaction(id) {
    return this.http.get<Transaction[]>(Config.SERVER_URL + '/transactions/' + id);
  }
}
