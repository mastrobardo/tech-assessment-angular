import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsdetailsComponent } from './transactionsdetails.component';
import { MoneyconverterModule } from 'src/moneyConverter/moneyconverter.module';
import { MatTableModule } from '@angular/material/table';
import { TransactionsDetailsService } from './transactionsdetails.service';
import { MatCardModule } from '@angular/material/card'

@NgModule({
  declarations: [TransactionsdetailsComponent],
  exports: [TransactionsdetailsComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MoneyconverterModule,
    MatCardModule
  ],
  providers: [TransactionsDetailsService]
})
export class TransactionsdetailsModule { }
