import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionsdetailsComponent } from './transactionsdetails.component';

describe('TransactionsdetailsComponent', () => {
  let component: TransactionsdetailsComponent;
  let fixture: ComponentFixture<TransactionsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
