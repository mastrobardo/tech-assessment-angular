import { Component, OnInit } from '@angular/core';
import { TransactionsDetailsService } from './transactionsdetails.service';
import { ActivatedRoute } from '@angular/router';
import { Transaction } from '../shared/interfaces/transactions.interface';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-transactionsdetails',
  templateUrl: './transactionsdetails.component.html',
  styleUrls: ['./transactionsdetails.component.scss']
})
export class TransactionsdetailsComponent implements OnInit {

  constructor(private transactionsService: TransactionsDetailsService, private route: ActivatedRoute) { }
  id: string;
  transactions: Transaction[]
  dataSource = new MatTableDataSource<Transaction>();
  displayedColumns: Array<string> = ['owner', 'orderCode', 'orderId', 'credit', 'balance'];
  
  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.transactionsService.fetchTransaction(this.id).subscribe(data => {
      // this.accounts = data;
      this.dataSource.data = data;
      console.log(data)
    });
  }

}
