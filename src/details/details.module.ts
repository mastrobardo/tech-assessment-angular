import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsComponent } from './details.component';
import { TransactionsdetailsModule } from './transactionsdetails/transactionsdetails.module';
import { UserdetailsModule } from './userdetails/userdetails.module';



@NgModule({
  declarations: [DetailsComponent],
  exports: [DetailsComponent],
  imports: [
    CommonModule,
    TransactionsdetailsModule,
    UserdetailsModule
  ]
})
export class DetailsModule { }
