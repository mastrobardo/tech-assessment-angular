export interface Transaction {
  readonly owner: string;
  readonly orderCode: string;
  readonly orderId: number;
  readonly credit: number;
  readonly balance: number;
}