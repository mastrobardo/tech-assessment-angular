import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserdetailsComponent } from './userdetails.component';
import { DatatableService } from 'src/datatable/datatable.service';
import { MatCardModule } from '@angular/material/card'
import { MoneyconverterModule } from 'src/moneyConverter/moneyconverter.module';


@NgModule({
  declarations: [UserdetailsComponent],
  exports: [UserdetailsComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MoneyconverterModule
  ],
  providers: [DatatableService]
})
export class UserdetailsModule { }
