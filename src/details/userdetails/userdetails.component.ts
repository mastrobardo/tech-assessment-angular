import { Component, OnInit } from '@angular/core';
import { DatatableService } from 'src/datatable/datatable.service';
import { ActivatedRoute } from '@angular/router';
import { Account } from '../../datatable/shared/interfaces/account.interface'
@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.scss']
})
export class UserdetailsComponent implements OnInit {

  constructor(private accountService: DatatableService, private route: ActivatedRoute) { }
  userInfo;
  id: string;
  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.accountService.fetchAccount(this.id).subscribe(data => {
      this.userInfo = data;
    });
  }

}
