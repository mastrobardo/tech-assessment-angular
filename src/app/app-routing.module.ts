import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DetailsComponent } from 'src/details/details.component';
import { DatatableComponent } from 'src/datatable/datatable.component';


const routes: Routes = [
  { path: 'accounts', component: DatatableComponent },
  { path: '', redirectTo: 'accounts',  pathMatch: 'full' },
  { path: 'details/:id', component: DetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
