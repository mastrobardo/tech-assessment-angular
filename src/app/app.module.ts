import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RatesModule } from '../rates/rates.module';
import { SocketService } from 'src/rates/shared/services/socket.service';
import { DatatableModule } from 'src/datatable/datatable.module';
import { HttpClientModule } from '@angular/common/http';
import { MoneyconverterModule } from 'src/moneyConverter/moneyconverter.module';
import { DetailsModule } from 'src/details/details.module';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RatesModule,
    // BrowserAnimationsModule,
    DatatableModule,
    MoneyconverterModule,
    DetailsModule
  ],
  providers: [SocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
