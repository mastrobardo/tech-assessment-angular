import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatesComponent } from './rates.component';
// import { MatToolbarModule } from '@angular/material/toolbar';


@NgModule({
  declarations: [RatesComponent],
  exports: [RatesComponent],
  imports: [
    CommonModule,
    // MatToolbarModule
  ]
})
export class RatesModule { }
