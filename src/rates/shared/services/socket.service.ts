import { Injectable, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Event, Config } from '../../../enums/enums';
import * as socketIo from 'socket.io-client';
import { EventEmitter } from '@angular/core';

@Injectable()
export class SocketService {
    private socket;
    @Output() rateChange: EventEmitter<number> = new EventEmitter<number>();
    public initSocket(): void {
        this.socket = socketIo(Config.SERVER_URL);
        this.socket.on('message', dt => {
            this.rateChange.emit(dt);
          });
    }
    public getRatesChangeEmitter() {
        return this.rateChange;
    }

    public onEvent(event: Event): Observable<any> {
        return new Observable<Event>(observer => {
            this.socket.on(event, () => observer.next());
        });
    }
}
