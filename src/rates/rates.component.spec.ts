import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatesComponent } from './rates.component';
import { SocketService } from './shared/services/socket.service';

describe('RatesComponent', () => {
  let component: RatesComponent;
  let fixture: ComponentFixture<RatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatesComponent ],
      providers: [SocketService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
