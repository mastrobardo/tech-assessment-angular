import { Component, OnInit } from '@angular/core';
import { Event } from '../enums/enums';
import { SocketService } from './shared/services/socket.service';
@Component({
  selector: 'app-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss']
})
export class RatesComponent implements OnInit {
  currentRate = 1;

  constructor(private socketService: SocketService) { }
  ngOnInit(): void {
    this.initIoConnection();
  }

  private initIoConnection(): void {
    this.socketService.initSocket();
    this.socketService.onEvent(Event.CONNECT)
    .subscribe(() => {
      console.log('connected');
    });
    this.socketService.onEvent(Event.DISCONNECT)
    .subscribe(() => {
      console.log('disconnected');
    });
    this.socketService.getRatesChangeEmitter()
    .subscribe(rate => this.changeRate(rate));
  }

  private changeRate(rate: number): void {
    this.currentRate = rate;
  }
}
