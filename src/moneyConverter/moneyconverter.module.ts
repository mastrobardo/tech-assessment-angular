import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoneyconverterComponent } from './moneyconverter.component';



@NgModule({
  declarations: [MoneyconverterComponent],
  exports: [MoneyconverterComponent],
  imports: [
    CommonModule
  ]
})
export class MoneyconverterModule { }
