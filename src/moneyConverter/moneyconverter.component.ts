import { Component, OnInit, Input } from '@angular/core';
import { SocketService } from 'src/rates/shared/services/socket.service';

@Component({
  selector: 'app-moneyconverter',
  templateUrl: './moneyconverter.component.html',
  styleUrls: ['./moneyconverter.component.scss']
})
export class MoneyconverterComponent implements OnInit {
  @Input()
  public btc: number;

  @Input()
  public inline: boolean = false;

  public dollars: number;

  constructor(private socketService: SocketService) { }

  ngOnInit(): void {
    this.dollars = this.btc;
    this.socketService.getRatesChangeEmitter()
    .subscribe(rate => {
      this.dollars = this.btc * rate;
      // typeof this.dollar === number -> gives true
      this.dollars = +this.dollars.toFixed(2); // weird, I need casting
    });
  }

}
