import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyconverterComponent } from './moneyconverter.component';
import { SocketService } from 'src/rates/shared/services/socket.service';

describe('MoneyconverterComponent', () => {
  let component: MoneyconverterComponent;
  let fixture: ComponentFixture<MoneyconverterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoneyconverterComponent ],
      providers: [SocketService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyconverterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
