import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DatatableComponent } from './datatable.component';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MoneyconverterModule } from 'src/moneyConverter/moneyconverter.module';
import { SocketService } from 'src/rates/shared/services/socket.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';

describe('DatatableComponent', () => {
  let component: DatatableComponent;
  let fixture: ComponentFixture<DatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatatableComponent ],
      imports: [
        CommonModule,
        MatTableModule,
        MoneyconverterModule,
        HttpClientModule
      ],
      providers: [HttpClient, SocketService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
