export interface Account {
  readonly name: string;
  readonly category: string;
  readonly tag: string;
  readonly balance: number;
  readonly availableBalance: number;
  readonly guid: string;
}