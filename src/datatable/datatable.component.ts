import { Component, OnInit, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { NgZone } from '@angular/core';
import {DatatableService} from './datatable.service';
@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss']
})
export class DatatableComponent implements OnInit {

  constructor(private accountsService: DatatableService, private router: Router, private zone: NgZone, private route: ActivatedRoute) { }
  accounts: Account[];
  dataSource = new MatTableDataSource<Account>();
  displayedColumns: Array<string> = ['name', 'category', 'tag', 'balance', 'availableBalance'];

fetchAccounts() {
  this.accountsService.fetchAccounts().subscribe(data => {
    // this.accounts = data;
    this.dataSource.data = data;
  });
}

ngOnInit(): void {
  this.fetchAccounts();
}
goToDetails(guid: string) {
  this.zone.run(() => { this.router.navigate(['/details', guid]); });
}


}
