import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../enums/enums';

@Injectable({
  providedIn: 'root'
})
export class DatatableService {

  constructor( private http: HttpClient ) { }

  fetchAccounts() {
    return this.http.get<Account[]>(Config.SERVER_URL + '/accounts');
  }
 
  fetchAccount(id) {
    return this.http.get<Account>(Config.SERVER_URL + '/accounts/' + id);
  }
}
