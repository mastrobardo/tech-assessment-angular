import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatatableComponent } from './datatable.component';
import { HttpClient } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MoneyconverterModule } from 'src/moneyConverter/moneyconverter.module';
import { SocketService } from 'src/rates/shared/services/socket.service';
import { DatatableService } from './datatable.service';
import { RatesModule } from 'src/rates/rates.module';

@NgModule({
  declarations: [DatatableComponent],
  exports: [DatatableComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MoneyconverterModule,
    RatesModule
  ],
  providers: [HttpClient, SocketService, DatatableService]
})
export class DatatableModule { }
