export enum Action {
  JOINED,
  LEFT,
  RENAME
}
export enum Event {
  CONNECT = 'connect',
  DISCONNECT = 'disconnect',
  MESSAGE = 'message'
}

export enum Config {
  SERVER_URL = 'http://localhost:3000'
}
